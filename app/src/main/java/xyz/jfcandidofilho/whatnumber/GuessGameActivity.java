package xyz.jfcandidofilho.whatnumber;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class GuessGameActivity extends AppCompatActivity {

    /**
     * TAG is an argument necessary for Log.d() to resolve the method being called. Useful in
     * debugging.
     * @since VersionCode 1
     */
    private static final String TAG = MainActivity.class.getSimpleName();

    private int remaining_guesses;
    private int number;

    private TextView status_view;
    private EditText guess_view;

    private Boolean canPlay;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {

        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_guessgame );

        generate_random_number( 0, 10 );

        remaining_guesses = 4;
        canPlay = true;

        status_view = (TextView) findViewById( R.id.txv_message_status );
        guess_view = (EditText) findViewById( R.id.edv_guess );

    }
    
    public void guess_number( View view ){

        int guess;
        String guess_string;

        if( remaining_guesses > 0 && canPlay ){

            guess_string = guess_view.getText().toString();

            if( ! guess_string.isEmpty() ) {

                remaining_guesses--;

                Log.d(TAG, "guess_number: remaining " + remaining_guesses );

                guess = Integer.parseInt( guess_string.toString() );

                Log.d(TAG, "guess_number: guess " + guess );

                if( guess == number ){

                    canPlay = false;

                    Log.d(TAG, "guess_number: true");

                    status_view.setText( getResources().getText( R.string.game_messages_win ) );

                } else {

                    Log.d(TAG, "guess_number: false");

                    if( guess > number )
                        status_view.setText( getResources().getText( R.string.game_messages_lesser) );
                    else
                        status_view.setText( getResources().getText( R.string.game_messages_greater ) );

                }

            } else status_view.setText( getResources().getText( R.string.game_messages_empty ) );

        } else {

            canPlay = false;

            status_view.setText( getResources().getText( R.string.game_messages_lose ) );

        }

    }

    public void restart( View view ){

        // Not pretty to have a flicker BUT easier. And I'm lazy. :D
        recreate();

    }

    private void generate_random_number( int start, int finish ){

        Random random;

        random = new Random();

        number = random.nextInt( finish );

        Log.d( TAG, "generate_random_number: " + number );

    }

}
